package justanotherchessgame.controller;

import java.io.File;

import javafx.scene.layout.Pane;

/**
 * This interface is interposed between user and view, and it's able to manage all the interactions between user and view.
 */
public interface MainMenuController {

    /**
     * This function is interposed between user and view, and its used to create the main menu view. 
     * @return the graphic content of the main menu.
     */
    Pane createMainMenuView();

    /**
     * This function is used to create a game with the currently set parameters.
     */
    void createGame();

    /**
     * Function used to resize the main menu view.
     */
    void resize();

    /**
     * Class setter for blitz mode.
     * @param mode is the game mode: classic or blitz.
     */
    void setBlitzMode(boolean mode);

    /**
     * Class setter for the game file.
     * @param gameFile is the loading file.
     */
    void setNew(File gameFile);

    /**
     * Class setter for the local player color.
     * @param color is the color of the local player.
     */
    void setColor(boolean color);

    /**
     * Class setter for the difficult of the AI.
     * @param ia is the difficult of the AI.
     */
    void setIA(int ia);
}
