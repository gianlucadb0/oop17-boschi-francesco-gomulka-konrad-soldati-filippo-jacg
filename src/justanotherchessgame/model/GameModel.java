package justanotherchessgame.model;

import java.util.List;

import justanotherchessgame.util.Point;

/**
 * Interface that represents the state of a chess game.
 */
public interface GameModel {
    /**
     * Function which returns the color of the next expected player.
     * @return a boolean representing the next expected player.
     */
    boolean nextColor();
    /**
     * Function which requests a certain move on the chessboard.
     * @param m is the move which is requested.
     */
    void requestMove(MoveInfo m);
    /**
     * Function which returns a list of possible moves given a certain point.
     * @param p is the point from which the moves will be calculated.
     * @return a list of all the possible moves from the given point.
     */
    List<MoveInfo> getValidMoves(Point p);
    /**
     * Function which returns the currently used board.
     * @return the currently used board.
     */
    ChessboardModel getCurrentBoard();
    /**
     * Function which returns all the performed moves during this game.
     * @return the list of all performed moves.
     */
    List<MoveInfo> getMoveHistory();
    /**
     * Function which loads a game from a list of moves.
     * @param moves is the list of all moves which have ti be applied.
     */
    void loadGame(List<MoveInfo> moves);
    /**
     * Function which returns the number of all performed moves during this game.
     * @return an integer representing the number of performed moves during current game.
     */
    int getMovesCount();

    /**
     * Starts the current game.
     */
    void start();

    /**
     * Method to know if the game is running.
     * @return the status of the game.
     */
    boolean isRunning();

    /**
     * Stops the current game.
     */
    void stopGame();
}
