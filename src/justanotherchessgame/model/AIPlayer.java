package justanotherchessgame.model;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import justanotherchessgame.util.GameResult;
import justanotherchessgame.util.Point;
import justanotherchessgame.ai.AIEngine;

/**
 * Class used to represent an AI player.
 */
public class AIPlayer extends Player {
    private ChessboardModel chessboard;
    private final AIEngine ai;
    private boolean running;

    /**
     * Class constructor.
     * @param color is the player color.
     * @param ai is the type of AI.
     */
    public AIPlayer(final boolean color, final AIEngine ai) {
        super(color);
        this.ai = ai;
    }

    @Override
    public final void notifyMove(final MoveInfo m) {
        chessboard.move(m);
        checkTurn();
    }

    /**
     * Function used to check the turn.
     */
    public final void checkTurn() {
        // Nothing to do if there is the game's already over
        if (!running) {
            return;
        }
        if (getGame().nextColor() == this.isWhite()) {
            CompletableFuture.supplyAsync(() -> ai.getMove(chessboard, this.isWhite())).thenAccept(move -> {
                if (move != null) {
                    getGame().requestMove(move);
                } else {
                    System.out.println("IA doesn't know what to do now");
                }
            });
        }
    }

    @Override
    public final void setGame(final GameModel gm) {
        super.setGame(gm);
    }

    @Override
    public final void requestMove(final MoveInfo m) {
        //AIs are not meant to request moves
    }

    @Override
    public final List<MoveInfo> possibleMoves(final Point p) {
        return MovesChecker.possibleMoves(chessboard, p);
    }

    @Override
    public final List<MoveInfo> getMoveHistory() {
        return this.getGame().getMoveHistory();
    }

    @Override
    public final void notifyResult(final GameResult result) {
        // We don't care about the result, we just stop playing
        running = false;
    }

    @Override
    public final void notifyStart() {
        // Gets current board and sets a flag to true
        running = true;
        chessboard = this.getGame().getCurrentBoard();
        checkTurn();
    }

}
