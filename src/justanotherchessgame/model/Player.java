package justanotherchessgame.model;

import java.util.List;

import justanotherchessgame.util.GameResult;
import justanotherchessgame.util.Point;

/**
 * Abstract Class representing a player.
 */
public abstract class Player {

    /**
     * The color of the player.
     */
    private final boolean color;
    /**
     * The game assigned to the player.
     */
    private GameModel game;
    /**
     * Player constructor.
     * @param color is the player color (white or black).
     */
    protected Player(final boolean color) {
        this.color = color;
    }
    /**
     * Setter of the players game.
     * @param gameModel is the current game.
     */
    public void setGame(final GameModel gameModel) {
        game = gameModel;
    }

    /**
     * Setter of the players game.
     * @return gameModel of the current game.
     */
    protected GameModel getGame() {
        return game;
    }

    /**
     * Getter of the players color.
     * @return a boolean representing the players color.
     */
    public boolean isWhite() {
        return this.color;
    }
    /**
     * Method which notifies a given move to a player.
     * @param m is the move that gets notified to the player.
     */
    public abstract void notifyMove(MoveInfo m);
    /**
     * Method which request a certain move to the game.
     * @param m is the move which has to be notified to the game.
     */
    public void requestMove(final MoveInfo m) {
        game.requestMove(m);
    }
    /**
     * Method which returns a list of possible moves given a certain point.
     * @param p is the point from which the moves have to start.
     * @return the list of possible moves.
     */
    public List<MoveInfo> possibleMoves(final Point p) {
        return game.getValidMoves(p);
    }

    /**
     * Method that returns the list of all the moves being done so far.
     * @return the list of the moves that were done so far.
     */
    public List<MoveInfo> getMoveHistory() {
        return game.getMoveHistory();
    }

    /**
     * Method to notify a player about the ending result of the game.
     * @param result is the result of the game..
     */
    public abstract void notifyResult(GameResult result);

    /**
     * Method used by the game to notify about the game start.
     */
    public abstract void notifyStart();

    /**
     * Method used by the player to exit a game.
     */
    public void exitGame() {
        game.stopGame();
    }
}
