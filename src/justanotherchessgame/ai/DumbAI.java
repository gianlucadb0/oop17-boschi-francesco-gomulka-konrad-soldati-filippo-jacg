package justanotherchessgame.ai;

import justanotherchessgame.model.MovesChecker;
import justanotherchessgame.model.ChessboardModel;
import justanotherchessgame.model.MoveInfo;
import justanotherchessgame.model.Piece;
import justanotherchessgame.model.Queen;

import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Class used to create a "dumb" AI.
 */
public class DumbAI implements AIEngine {

    @Override
    public final MoveInfo getMove(final ChessboardModel cb, final boolean color) {
        final List<Piece> pieces = cb.getPieceOnBoard().stream().filter(x -> x.isWhite() == color).collect(Collectors.toList());
        Collections.shuffle(pieces);
        for (final Piece p : pieces) {
            final List<MoveInfo> lm = MovesChecker.possibleMoves(cb, p.getPoint());
            if (!lm.isEmpty()) {
                final MoveInfo chosen = lm.get(new Random().nextInt(lm.size()));
                if (MovesChecker.willPromote(p, chosen.getTo())) {
                    chosen.setPromotion(Queen.class);
                }
                return chosen;
            }
        }
        //If no moves were found, returns a null reference
        return null;
    }
}
