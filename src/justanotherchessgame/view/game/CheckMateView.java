package justanotherchessgame.view.game;

/**
 * Interface used to create and show the game ending view.
 */
public interface CheckMateView {
    /**
     * Function used to show the game ending view after the creation.
     */
    void showCheckMateView();
}
